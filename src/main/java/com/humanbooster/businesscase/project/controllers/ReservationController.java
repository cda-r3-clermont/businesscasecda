package com.humanbooster.businesscase.project.controllers;

import com.humanbooster.businesscase.project.forms.EmplacementForm;
import com.humanbooster.businesscase.project.forms.ReservationForm;
import com.humanbooster.businesscase.project.services.ReservationService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;

@Controller
@RequestMapping("/reservations")
public class ReservationController {

    @Autowired
    ReservationService reservationService;

    @Autowired
    Validator validator;

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public ModelAndView addReservation(){
        // Création d'un model and view qui retourne le template
        ModelAndView mv = new ModelAndView("reservations/form");

        // Objet qui permet de créer un formulaire de reservation
        ReservationForm rf = new ReservationForm();

        // On ajoute notre objet de reservation
        mv.addObject("reservationForm", rf);

        // On retourne notre template
        return mv;
    }

    // Soumission du formulaire de reservation
    // Il prend en paramètre un objet ReservationForm
    // On ne met pas le @Valid sur ReservationForm puisque nous le validerons plus tard
    // En effet, nous ajouterons notre collection d'emplacement avant la validation
    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String addReservation(  ReservationForm reservationForm,
                                 BindingResult bindingResult,
                                 @RequestBody String postPayload, Model model){

        // J'appel mon service de reservation pour lui demander de transformer le body
        // de ma requête HTTP en objet en list d'emplacement
        List<EmplacementForm> listEmplacementForm = this.reservationService.payloadToResas(postPayload);

        // La liste d'emplacement réccupérée depuis mon service je l'ajoute à l'objet commande de mon formulaire
        reservationForm.setEmplacements(listEmplacementForm);

        // Permet de revalider notre champs reservation Form
        // Nous l'avons modifié donc il faut revalider.
        DataBinder binder = new DataBinder(reservationForm);

        // Etant donné que j'ai modifié l'objet que représente mon formulaire,
        // Je demande à la validation spring de revalider mes données
        // Si jamais les emplacements que j'ai ajouté ne sont pas valides
        // Les erreurs seront affiché dans mon formullaire
        binder.setValidator(validator);
        // Je demande de revalider mon formulaire avec le nouvel objet reservationForm
        binder.validate(reservationForm, "reservationForm");
        // Je réccupére les nouvelles erreurs
        bindingResult = binder.getBindingResult();

        if (bindingResult.hasErrors()) {
            model.addAttribute("fields", bindingResult);
            // On ajoute notre objet de reservation
            model.addAttribute("reservationForm", reservationForm);

           return "reservations/form";
        } else {
            // Envoyer les données en BDD
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String currentPrincipalName = authentication.getName();

            this.reservationService.persistReservationFromForm(reservationForm, currentPrincipalName);

            return "redirect:/";
        }

    }
}


