package com.humanbooster.businesscase.project.forms;

import com.humanbooster.businesscase.project.constraint.EnumConstraint;
import com.humanbooster.businesscase.project.enums.EquipementEnum;
import com.humanbooster.businesscase.project.validation_groups.RevalidateReservation;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;

public class EmplacementForm {

    @Min(value = 1, message = "La première file est la file n°1")
    @Max(value = 8, message = "La dernière file est la file n°8")
    private Integer file;

    // On ajoute une contrainte qui vérifie une énum
    // On lui spécifie l'énum que l'on souhaite vérifier
    // On lui spécifie le message d'erreur
    @EnumConstraint(targetClassType = EquipementEnum.class, message="Nous ne proposons pas cet équipement !")
    private String equipement;

    public Integer getFile() {
        return file;
    }

    public void setFile(Integer file) {
        this.file = file;
    }

    public String getEquipement() {
        return equipement;
    }

    public void setEquipement(String equipement) {
        this.equipement = equipement;
    }
}
