package com.humanbooster.businesscase.project.forms;

import com.humanbooster.businesscase.project.constraint.DateOrderConstraint;
import com.humanbooster.businesscase.project.constraint.OpeningConstraint;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

// Contrainte qui permet de s'assurer que la date de début est bien avant la date de fin
@DateOrderConstraint(
        fieldStart = "dateDebut",
        fieldEnd = "dateFin"
)
public class ReservationForm {

    // Date de début de la résa
    // L'annotation DateTimeFormat permet de convertir un string en date
    // Le format HTML par défaut est le format yyyy-MM-dd
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    // Je vérifie que la date de départ n'est pas nulle
    @NotNull
    // Je vérifie que la date est dans le future
    // Empêche une reservation sur le jour même
    // Empêche également de reserver pour une date qui est déjà passée
    @Future(message = "Cette date est déjà passée ...")
    // Contrainte custom rajoutée qui permet de vérifier que l'on est bien en période d'ouverture
    @OpeningConstraint(message = "La date de début n'est pas dans nos jours d'ouverture")
    private Date dateStart;

    // Date de fin de la résa
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    @Future(message = "Cette date est déjà passée ...")
    @OpeningConstraint()
    private Date dateEnd;

    // Remarque
    private String remarque;

    // Liste emplacement
    @Valid
    private List<EmplacementForm> emplacements;

    public ReservationForm() {
        this.emplacements = new ArrayList<>();
        this.emplacements.add(new EmplacementForm());
    }

    public void setEmplacements(List<EmplacementForm> emplacements) {
        this.emplacements = emplacements;
    }

    public List<EmplacementForm> getEmplacements(){
        return  this.emplacements;
    }

    public void addEmplacement(EmplacementForm ef){
        this.emplacements.add(ef);
    }

    public void removeEmplacement(EmplacementForm ef){
        this.emplacements.remove(ef);
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }
}
