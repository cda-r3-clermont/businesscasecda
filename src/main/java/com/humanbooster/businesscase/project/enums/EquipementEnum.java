package com.humanbooster.businesscase.project.enums;

// Affiche les différentes valeurs possible
// pour nos équipements
public enum EquipementEnum {
    UN_LIT,
    DEUX_LITS,
    UN_FAUTEUIL,
    DEUX_FAUTEUILS
}
