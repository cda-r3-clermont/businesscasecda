package com.humanbooster.businesscase.project.constraint;

import com.humanbooster.businesscase.project.forms.ReservationForm;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

// Ma classe qui va valider mes dates. En paramètre DateOrderContraint qui correspond à l'annotation créé précédement
// Object qui correspond à l'objet sur lequel j'applique ma validation (dans notre cas un objet ReservationForm)
public class DateOrderConstraintValidator implements ConstraintValidator<DateOrderConstraint, Object> {
    // Pas touché : il est obligatoire sinon erreur (interface ContraintValidator)
    @Override
    public void initialize(DateOrderConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    // Méthode isValid qui retournera un booléen
    // true si dateDébut <= date de fin
    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        // Je réccupére mon objet puis je le cast en objet ReservationForm
        ReservationForm reservationForm = (ReservationForm) o;
        // Si les 2 dates ne sont pas nulles
        if(reservationForm.getDateEnd() != null && reservationForm.getDateEnd() != null){
            // Je retourne true si la date de début est avant ou égale à la date de fin
            // J'utilise la méthode getTime de mon objet date qui retourne le timestamp
            return reservationForm.getDateStart().getTime()
                    <= reservationForm.getDateEnd().getTime();
        } else {
            return true;
        }

    }
}
