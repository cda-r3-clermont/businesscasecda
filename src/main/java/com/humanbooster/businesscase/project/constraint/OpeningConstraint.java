package com.humanbooster.businesscase.project.constraint;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// Contrainte qui permet de s'assurer que les dates saisies dans le formulaire de reservation
// sont bien dans les dates d'ouverture

// On spécifie la classe qui va permettre de valider cette contrainte
@Constraint(validatedBy = OpeningConstraintValidator.class)
// Cette contrainte sera appliquée eu nos champs de type date
// C'est donc une contrainte de type FIELD
@Target({ElementType.FIELD})
// Les erreurs rencontrées ne seront retenues que pendant la requête HTTP
// Elle seront oublié lors de la prochaine requête
@Retention(RetentionPolicy.RUNTIME)
// Déclaration de notre contrainte avec le mot clé @interface
public @interface OpeningConstraint {
    // Nous retrouvons ici notre messaage par défaut
    String message() default "Nous sommes fermés à cette date ...";
    // On retrouve les groupes sur lesquels la contrainte est appliquée
    Class<?>[] groups() default {};
    // On retrouve l'élément sur lequel notre contrainte est appliquée (le champ date dans notre requête)
    Class<? extends Payload>[] payload() default {};

}
