package com.humanbooster.businesscase.project.constraint;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// La classe de validation (que l'on va commenter juste après )
@Constraint(validatedBy = DateOrderConstraintValidator.class)
// Ici, nous ne validerons pas un champ précis mais 2 champs de notre model.
// J'utilise donc un champ de type TYPE
@Target({ElementType.TYPE})
// Comme pour la contrainte précédents, elle sera valable durant la requête HTTP courante
@Retention(RetentionPolicy.RUNTIME)
public @interface DateOrderConstraint {
    // Je retrouve le message par défaut
    String message() default "La date de début doit être avant la date de fin !";

    // Appliquer par défaut sur tout les groupes
    Class<?>[] groups() default {};
    
    // Contenu de ma requête
    Class<? extends Payload>[] payload() default {};

    // Field qui correspond à ma date de fin
    String fieldEnd();

    // Field start qui correspond à ma date de début
    String fieldStart();
}
