package com.humanbooster.businesscase.project.repositories;

import com.humanbooster.businesscase.project.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
     User findByEmail(String email);
}
