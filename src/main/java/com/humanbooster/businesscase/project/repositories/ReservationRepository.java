package com.humanbooster.businesscase.project.repositories;

import com.humanbooster.businesscase.project.models.Reservation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long> {
}
