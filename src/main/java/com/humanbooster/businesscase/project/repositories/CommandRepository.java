package com.humanbooster.businesscase.project.repositories;

import com.humanbooster.businesscase.project.models.Command;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommandRepository extends CrudRepository<Command, Long> {
}
