package com.humanbooster.businesscase.project.services;

import com.humanbooster.businesscase.project.models.User;
import com.humanbooster.businesscase.project.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User findByEmail(String email){
        return this.userRepository.findByEmail(email);
    }
}
